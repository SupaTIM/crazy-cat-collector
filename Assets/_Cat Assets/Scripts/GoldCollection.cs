﻿using UnityEngine;
using System.Collections;

public class GoldCollection : MonoBehaviour {
	public GameObject[] GoldCoin;
	public EconSystem econSystem;
	public AvailableCoordinates availableCoordinates;
	public AudioClip GoldSound;
	public AudioClip[] PickUpGoldSound;
	public GameObject SpeechObject;

	// Use this for initialization
	void Start () {
		StartCoroutine(MoneyDrop());
	}
	
	// Update is called once per frame
	void Update () {

	}


	void OnTriggerEnter (Collider collider) {
		if (collider.tag == "GoldTag" ) {
			collider.gameObject.transform.position = new Vector3(-18.0f, 0, -14f);
			econSystem.goldTotal += 50;
			iTween.Stab(gameObject, GoldSound, 0);

			int soundChance = Random.Range(0, 4);
			if (soundChance != 0) {
				int clipIndex = Random.Range(0, PickUpGoldSound.Length);
				iTween.Stab(SpeechObject, iTween.Hash("audioclip", PickUpGoldSound[clipIndex], "delay", 0));
			}
		}
	}

	public void DropGold() {
		SpawnGold(availableCoordinates.indexLength, 0);
	}

	IEnumerator MoneyDrop() {
		yield return new WaitForSeconds(Random.Range(5,20));
		Debug.Log ("MoneyDrop: "+Time.time);
		DropGold();
		StartCoroutine(MoneyDrop());
	}

	void SpawnGold (int indexLength, int goldIndex) {
		if (goldIndex > 3) {
			Debug.Log("No Available Gold");
			return;
		}
		if (GoldCoin[goldIndex].transform.position != new Vector3(-18.0f, 0, -14f)) {
			Debug.Log("coin is already placed");
			SpawnGold(indexLength, goldIndex+1);
		}
		else {
			int randomIndex = Random.Range(0, indexLength);
			GoldCoin[goldIndex].transform.position = availableCoordinates.RandomAvailablePosition();
		}
	}
}
