﻿using UnityEngine;
using System.Collections;

public class RatTapKill : MonoBehaviour {

	public float hp = 5;
	public Vector3 defaultPosition;
	public bool invincibleFrames;
	public float invincibleTime = 0.5f;
	public AudioClip[] squeak;
	public EconSystem econSystem;

	public AudioClip[] RatDeath;
	public GameObject SpeechObject;

	// Use this for initialization
	void Start () {
		defaultPosition = gameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Health ();
	}

	public void Tapped () {
		if (!invincibleFrames) {
			hp -=1;
			int clipIndex = Random.Range(0, squeak.Length);
			iTween.Stab(gameObject, iTween.Hash("audioclip", squeak[clipIndex], "delay", 0));
			StartCoroutine(RatHurt ());
		}
	}

	void Health () {
		if (hp <= 0) {
			econSystem.ratsDead += 1;
			hp = 5;
			Debug.Log ( "Rats Killed = " + econSystem.ratsDead.ToString() );
			transform.position = defaultPosition;
			// allow seeker to move is off
			int clipIndex = Random.Range(0, RatDeath.Length);
			iTween.Stab(SpeechObject, iTween.Hash("audioclip", RatDeath[clipIndex], "delay", 0));
		}
	}

	void OnTriggerEnter (Collider collider) {
		if (collider.tag == "Cat") {
			hp -= 3;
			int clipIndex = Random.Range(0, squeak.Length);
			iTween.Stab(gameObject, iTween.Hash("audioclip", squeak[clipIndex], "delay", 0));
		}
	}

	IEnumerator RatHurt () {
		invincibleFrames = true;
		yield return new WaitForSeconds (invincibleTime);
		invincibleFrames = false;
	}
}
