﻿using UnityEngine;
using System.Collections;

public class AvailableCoordinates : MonoBehaviour {
	private Vector3[] availablePositons = new Vector3[110];
	public int indexLength;


	// Use this for initialization
	void Start () {
		indexLength = 0;
		Vector3 down = Vector3.down;
		float raycastLength = 3;

		for (int x = -16; x <= 18; x+=2) {
			for (int z = 2; z >= -14; z-=2) {
				RaycastHit hit;
				
				if (Physics.Raycast (new Vector3 (x, 2, z), down, out hit, raycastLength)) {
					if (hit.collider.CompareTag("Ground Tile")) {
						// Add to the available coordinates
						availablePositons[indexLength] = new Vector3 (x, 0, z);
						indexLength++;
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public Vector3 RandomAvailablePosition () {
		Vector3 randomPosition = Vector3.zero;
		int randomIndex = 0;

		Vector3 down = Vector3.down;
		float raycastLength = 3;
		RaycastHit hit;

		while (randomPosition == Vector3.zero) {
			randomIndex = Random.Range(0, indexLength);
			if (Physics.Raycast (new Vector3 (availablePositons[randomIndex].x, 2, availablePositons[randomIndex].z), down, out hit, raycastLength)) {
				if (hit.collider.CompareTag("Ground Tile")) {
					randomPosition = availablePositons[randomIndex];
				}
			}
		}

		return availablePositons[randomIndex];
	}
}
