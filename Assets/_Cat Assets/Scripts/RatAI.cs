﻿using UnityEngine;
using System.Collections;
//Note this line, if it is left out, the script won't know that the class 'Path' exists and it will throw compiler errors
//This line should always be present at the top of scripts which use pathfinding
using Pathfinding;

public class RatAI : MonoBehaviour {
	//The point to move to
	public Vector3 targetPosition;
	
	private Seeker seeker;
	private CharacterController controller;
	
	//The calculated path
	public Path path;
	
	//The AI's speed per second
	public float speed = 100;
	
	//The max distance from the AI to a waypoint for it to continue to the next waypoint
	public float nextWaypointDistance = 3;
	
	//The waypoint we are currently moving towards
	private int currentWaypoint = 0;
	public EconSystem econSystem;
	public AvailableCoordinates availableCoordinates;

	public AudioClip[] MoneySteal;
	public AudioClip[] MeatSteal;
	
	public Vector3 grammaDirection;
	public GrammaStartPosition grammaStart;
	
	public void Start () {
		seeker = GetComponent<Seeker>();
		controller = GetComponent<CharacterController>();
		//Start a new path to the targetPosition, return the result to the OnPathComplete function
		seeker.StartPath (transform.position,targetPosition, OnPathComplete);
		StartCoroutine(NewCoordinate());
	}
	
	public void Update () {
		if (path == null) {
			//We have no path to move after yet
			return;
		}
		
		if (currentWaypoint >= path.vectorPath.Count) {
			return;
		}
		
		//Direction to the next waypoint
		Vector3 dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
		dir *= speed * Time.fixedDeltaTime;
		controller.SimpleMove (dir);
		
		grammaDirection = dir;
		
		//Check if we are close enough to the next waypoint
		//If we are, proceed to follow the next waypoint
		if (Vector3.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
			currentWaypoint++;
			return;
		}
	}

	IEnumerator NewCoordinate () {
		yield return new WaitForSeconds (Random.Range(2, 6));
		targetPosition = availableCoordinates.RandomAvailablePosition();
		seeker.StartPath (transform.position,targetPosition, OnPathComplete);
		StartCoroutine(NewCoordinate());
	} 
	
	public void OnPathComplete (Path p) {
		Debug.Log ("Yey, we got a path back. Did it have an error? "+p.error);
		if (!p.error) {
			path = p;
			//Reset the waypoint counter
			currentWaypoint = 0;
		}
	}

	void OnTriggerEnter (Collider collider) {
		if (collider.tag == "GoldTag") {
			targetPosition = collider.gameObject.transform.position;
			seeker.StartPath (transform.position,targetPosition, OnPathComplete);

			int clipIndex = Random.Range(0, MoneySteal.Length);
			iTween.Stab(gameObject, iTween.Hash("audioclip", MoneySteal[clipIndex], "delay", 0));
			StopAllCoroutines();
			StartCoroutine(NewCoordinate());
			collider.gameObject.transform.position = new Vector3(-18.0f, 0, -14f);
		}

		if (collider.tag == "Food") {
			targetPosition = collider.gameObject.transform.position;
			seeker.StartPath (transform.position,targetPosition, OnPathComplete);

			int clipIndex = Random.Range(0, MeatSteal.Length);
			iTween.Stab(gameObject, iTween.Hash("audioclip", MeatSteal[clipIndex], "delay", 0));
			StopAllCoroutines();

			StartCoroutine(NewCoordinate());
			collider.gameObject.transform.position = new Vector3(-18.0f, 0, -4f);
		}

		if (collider.tag == "Player") {
			grammaStart.health -= 10;
		}

	}
}
