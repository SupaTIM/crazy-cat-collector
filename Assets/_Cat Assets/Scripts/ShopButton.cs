﻿using UnityEngine;
using System.Collections;

public class ShopButton : MonoBehaviour {
	
	
	//Toggle for if shop menu is shown. Will alternate OnClick
	bool showMenu = false;
	GameObject[] menubuttons;
	// Use this for initialization
	void Start () {
		menubuttons = GameObject.FindGameObjectsWithTag("Shopmenu");
		foreach( GameObject temp in menubuttons){
			NGUITools.SetActive(temp,false);
		}
	}
	
	void OnClick(){
		showMenu = !showMenu;
		if(showMenu){
		foreach( GameObject temp in menubuttons){
			NGUITools.SetActive(temp,true);
			}
		}else{
			foreach( GameObject temp in menubuttons){
			NGUITools.SetActive(temp,false);
			}
		}
	}
}
