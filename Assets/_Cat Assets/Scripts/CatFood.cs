﻿using UnityEngine;
using System.Collections;

public class CatFood : MonoBehaviour {
	public bool holdingFood;
	public GameObject foodHeld;

	public AudioClip[] PickUpMeatSound;
	public AudioClip[] DropMeatSound;
	public AudioClip[] RunPastRat;
	public GameObject SpeechObject;
	public EconSystem econSystem;
	public GrammaStartPosition grammaStart;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (foodHeld != null)
			foodHeld.transform.position = gameObject.transform.position;
	}
	
	void OnTriggerEnter (Collider collider) {
		if (collider.tag == "Food" && !foodHeld && !holdingFood) {
			holdingFood = true;
			foodHeld = collider.gameObject;
			foodHeld.tag = "Untagged";

			int soundChance = Random.Range(0, 4);
			if (soundChance != 0) {
				int clipIndex = Random.Range(0, PickUpMeatSound.Length);
				iTween.Stab(SpeechObject, iTween.Hash("audioclip", PickUpMeatSound[clipIndex], "delay", 0));
			}
		}
		if (collider.tag == "Bowl" && foodHeld != null) {
			collider.tag = "FoodBowl";
			holdingFood = false;
			foodHeld.transform.position = collider.gameObject.transform.position;

			foodHeld.tag = "CatFood";

			foodHeld = null;



			int soundChance = Random.Range(0, 4);
			econSystem.food += 1;
			if (soundChance != 0) {
				int clipIndex = Random.Range(0, DropMeatSound.Length);
				iTween.Stab(SpeechObject, iTween.Hash("audioclip", DropMeatSound[clipIndex], "delay", 0));
			
			}
		}
		if (collider.tag == "Rat") {
			//grammaStart.health -= 10;
			int soundChance = Random.Range(0, 4);
			if (soundChance != 0) {
				int clipIndex = Random.Range(0, RunPastRat.Length);
				iTween.Stab(SpeechObject, iTween.Hash("audioclip", RunPastRat[clipIndex], "delay", 0));

			}
		}
	}
}
