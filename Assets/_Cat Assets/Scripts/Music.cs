﻿using UnityEngine;
using System.Collections;

public class Music : MonoBehaviour {

	public AudioClip[] musicList;

	// Use this for initialization
	void Start () {
		int musicIndex = Random.Range(0, musicList.Length);
		iTween.Stab(gameObject, iTween.Hash("audioclip", musicList[musicIndex], "delay", 3f, "oncomplete", "NextSong"));
	}
	
	// Update is called once per frame
	void Update () {

	}

	void NextSong () {
		int musicIndex = Random.Range(0, musicList.Length);
		iTween.Stab(gameObject, iTween.Hash("audioclip", musicList[musicIndex], "delay", 1f, "oncomplete", "NextSong"));
	}
}
