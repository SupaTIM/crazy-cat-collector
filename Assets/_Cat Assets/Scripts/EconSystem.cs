﻿using UnityEngine;
using System.Collections;

public class EconSystem : MonoBehaviour {
	public int goldTotal = 0; // total cash in your bank
	public int food; //amount of catfood you've purchase
	public int goldRate = 5; // rate of cash per minute
	public int incomeInterval = 20;
	public int ratsDead = 0; //total amount of rats youve killed.
	public int catsNumber = 1; //number of cats owned.

	public GameObject[] tastyBowls = new GameObject[4];
	public Vector3[] tastyBowlPositions = new Vector3[3];

	public UILabel bank;
	//public UILabel goldRateLabel;
	public UILabel ratsDeadLabel;
	public UILabel catsOwned;
	public UILabel foodPurchased;
	// Use this for initialization
	void Start () {
		StartCoroutine(GetMoneyMoney());
		for (int i = 0; i < 4; i++) tastyBowls[i].transform.position = tastyBowlPositions[i];
	}
	
	// Update is called once per frame
	void Update () {
		bank.text = goldTotal.ToString();
		//goldRateLabel.text = "GPM: " + goldRate.ToString();
		ratsDeadLabel.text = ratsDead.ToString();
		catsOwned.text = catsNumber.ToString();
		foodPurchased.text = food.ToString();
	}
	
	IEnumerator GetMoneyMoney () {
		yield return new WaitForSeconds(incomeInterval);
		goldTotal += goldRate;
		StartCoroutine(GetMoneyMoney());
	}
	
	void PickUpMoney (int amount) {
		goldTotal += amount;
	}
	
	void SpendMoney (int amount) {
		if (amount > goldTotal) {
			return; //insufficient funds
		}
		else {
			goldTotal -= amount;
		}
	}
}
