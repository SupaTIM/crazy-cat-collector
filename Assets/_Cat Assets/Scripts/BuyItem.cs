﻿using UnityEngine;
using System.Collections;

public class BuyItem : MonoBehaviour {
	public EconSystem econSystem;
	public int item = 0;
	public int catCost = 0;
	public int foodCost = 0;
	public int GRcost = 1;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnClick(){
		if(gameObject.name.Equals("Cat Button")){
			item = 50 + catCost;
			if(hasEnough()){
				catCost += 20;
				econSystem.goldTotal -= item;
			}
		}else if(gameObject.name.Equals("Food Button")){
			item = 20 + foodCost;	
			if(hasEnough()){
				foodCost +=20;
				econSystem.food+=5;
				econSystem.goldTotal -= item;
			}
		}else if(gameObject.name.Equals ("Increase Gold Rate")){
			item = 100 + GRcost;
			if(hasEnough()){
				GRcost = GRcost*2;
				econSystem.goldRate +=2;
				econSystem.goldTotal -= item;
			}
		}

	}
	
	public bool hasEnough(){
		return (econSystem.goldTotal > item);
	}
}
