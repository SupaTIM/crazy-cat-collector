﻿using UnityEngine;
using System.Collections;

public class ButtonFood : MonoBehaviour {
	public GameObject[] tastyMeat;
	public GameObject[] gatos;
	public CatAI catAI;

	public AudioClip[] TooMuchMeat;
	public GameObject SpeechObject;

	public AvailableCoordinates availableCoordinates;
	public int foodCost = 20;
	public int catCost = 50;
	public EconSystem econSystem;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void BuyFood() {
		if(hasEnough(foodCost)){
			econSystem.goldTotal -= foodCost;
			SpawnMeat(availableCoordinates.indexLength, 0);
		}
	}
	
	public void BuyCat(){
		if(hasEnough(catCost)){
			econSystem.goldTotal -= catCost;
			SpawnCat (0);
			econSystem.catsNumber += 1;
		}
	}

	void SpawnMeat (int indexLength, int meatIndex) {
		if (meatIndex > 3) {
			Debug.Log("No Available Meat");

			int clipIndex = Random.Range(0, TooMuchMeat.Length);
			iTween.Stab(SpeechObject, iTween.Hash("audioclip", TooMuchMeat[clipIndex], "delay", 0));
		
			return;
		}

		if (tastyMeat[meatIndex].transform.position != new Vector3(-18.0f, 0f, -4.0f)) {
			SpawnMeat(indexLength, meatIndex+1);
		}
		else {
			tastyMeat[meatIndex].transform.position = availableCoordinates.RandomAvailablePosition();
		}
	}

	void SpawnCat (int catIndex) {
		if (catIndex > 6) {
			Debug.Log("No Available Meat");
			return;
		}
		if (gatos[catIndex].transform.position != new Vector3(-20.0f, 1f, -8.0f)) {
			SpawnCat(catIndex+1);
		}
		else {
			gatos[catIndex].transform.position = new Vector3(-18.0f, 1f, -8.0f);
			catAI = gatos[catIndex].GetComponent("CatAI") as CatAI;
			catAI.enabled = true;
		}
	}
	
	
	public bool hasEnough(int cost){
		return (econSystem.goldTotal >= cost);
	}
}
